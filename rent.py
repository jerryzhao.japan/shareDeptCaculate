import datetime
import electricity_etc as etc

rent_file = "source/rent_fee.txt"
person_file = "source/rent_person.txt"


def get_room_fee():
    """Read rent fee file. Get how many rooms and the room price."""
    return etc.read_files(rent_file)


def get_people_live_info():
    """Read person information, get who lives in which room ,and who share room with others."""
    return etc.read_files(person_file)


def get_first_day(month):
    """Get first day of the month."""
    return datetime.datetime.strptime(month + '01', '%Y%m%d')


def get_last_day(month):
    """Get last day of the month. Add one month and minus one day."""
    year_month = datetime.datetime.strptime(month, '%Y%m')
    year = year_month.year + int(year_month.month / 12)
    month = year_month.month % 12 + 1
    return datetime.datetime(year, month, 1) - datetime.timedelta(days=1)


def get_day_of_person(date, room_number):
    num = 0
    for person_list in get_people_live_info():
        fr = datetime.datetime.strptime(person_list[2], '%Y%m%d')
        to = datetime.datetime.strptime(person_list[3], '%Y%m%d')
        if fr <= date < to and room_number == person_list[1]:
            num += 1
    return num


def get_room_price(room_number):
    """Get room price by file content."""
    for room_list in get_room_fee():
        if room_number == room_list[0]:
            return room_list[1]


def calculate_every_month(month_list, p_list):
    """Calculate every person's money in every month"""
    new_p_list = p_list
    for month in month_list:
        first_day = get_first_day(month)
        last_day = get_last_day(month)
        month = datetime.datetime.strptime(month, '%Y%m')
        improve_date = first_day
        personal_rent_fee = 0
        room_price = int(get_room_price(p_list[1]))
        per_day_price = room_price / etc.get_days_of_month(month.year, month.month)
        fr = datetime.datetime.strptime(p_list[2], '%Y%m%d')
        to = datetime.datetime.strptime(p_list[3], '%Y%m%d')
        while improve_date <= last_day:
            if fr <= improve_date < to:
                persons = get_day_of_person(improve_date, p_list[1])
                personal_rent_fee = personal_rent_fee + per_day_price / persons
            improve_date = improve_date + datetime.timedelta(days=1)
        new_p_list.append(float('%.1f' % personal_rent_fee))
    return new_p_list


def calculate_rent(month_list):
    """Calculate rent fee."""
    result_list = []
    str_day = datetime.date.today().strftime('%Y%m%d')
    person_rent_file = 'result/rent_' + str_day + '.txt'
    for person in get_people_live_info():
        result_list.append(calculate_every_month(month_list, person))
    etc.write_to_file(result_list, person_rent_file)


if __name__ == "__main__":
    # calculate_rent(['201708', '201709'])
    calculate_rent(['201708', '201709'])



