import calendar
import datetime

money_file_name = 'source/other_fee.txt'
people_file_name = 'source/other_person.txt'


def split_rent_fee(year, month, money=35000):
    days = get_days_of_month(year, month)
    print(days)
    return money / days


def get_days_of_month(year, month):
    """Get how many days in a month."""
    assert isinstance(month, int)  # input check
    assert isinstance(year, int)  # input check
    return calendar.monthrange(year, month)[1]


def get_live_days(time_from, time_to):
    """Get how many days the person lives in the department."""
    assert isinstance(time_from, str)
    assert isinstance(time_to, str)
    live_from = datetime.datetime.strptime(str(time_from), '%Y%m%d')
    live_to = datetime.datetime.strptime(str(time_to), '%Y%m%d')
    return (live_to - live_from).days


def read_files(file_name):
    """Read money file and person file. The file has fixed format.
    Just look for the example files : other_fee.txt and other_person.txt"""
    res_list = []
    with open(file_name) as f:
        for line in f:
            num_list = line.strip('\n').split(",")
            res_list.append(num_list)
    return res_list


def deal_with_money_list(money_list):
    """Deal money list , add the money covers how many days and how much for each day."""
    new_list = []
    for line_list in money_list:
        days = get_live_days(str(line_list[1]), str(line_list[2]))
        money_per_day = float('%.2f' % (int(line_list[0]) / days ))
        line_list.append(days)
        line_list.append(money_per_day)
        new_list.append(line_list)
    return new_list


def read_people_file():
    """Read person live information file. File name is a global variable."""
    return read_files(people_file_name)


def read_money_file():
    """Read money detail file. File name is a global variable."""
    return read_files(money_file_name)


def get_day_of_people_num(date):
    """Input a day, return how many person lived in that day."""
    num = 0
    for person_list in read_people_file():
        fr = datetime.datetime.strptime(person_list[1], '%Y%m%d')
        to = datetime.datetime.strptime(person_list[2], '%Y%m%d')
        if fr < date < to:
            num += 1
    return num


def deal_every_persons_money(person_list, new_money_list):
    """Calculate every person's money, person list is from file reading."""
    new_person_list = []
    for single in person_list:
        money = deal_single_person(single, new_money_list)
        single.append(str(money))
        new_person_list.append(single)
    return new_person_list


def deal_single_person(single_list, new_money_list):
    """Calculate one person's money, this function is called by deal_with_every_persons_money"""
    date_from = datetime.datetime.strptime(single_list[1], '%Y%m%d')
    date_to = datetime.datetime.strptime(single_list[2], '%Y%m%d')
    whole_money = 0
    while date_from < date_to:
        date_from = date_from + datetime.timedelta(days=1)
        for money_list in new_money_list:
            m_time_from = datetime.datetime.strptime(money_list[1], '%Y%m%d')
            m_time_to = datetime.datetime.strptime(money_list[2], '%Y%m%d')
            if m_time_from < date_from <= m_time_to:
                person_number = get_day_of_people_num(date_from)
                whole_money = whole_money + int(money_list[4]) / person_number
    return float('%.2f' % whole_money)


def write_to_file(list_name, file_name):
    """Write list to file. Add a enter in each end of line."""
    file = open(file_name, 'w')
    for line in list_name:
        file.writelines(','.join(str(e) for e in line))
        file.writelines('\n')
    file.close()


def calculate_money():
    """Main function to call all the function."""
    str_day = datetime.date.today().strftime('%Y%m%d')
    money_result_file = 'temp/temp_money_' + str_day + '.txt'
    person_result_file = 'result/others_' + str_day + '.txt'
    money_list = read_money_file()  # Read money and effective date from file.
    person_list = read_people_file()  # Read person and live date from file.
    new_money_list = deal_with_money_list(money_list)  # Process money file, add days last and money per day.
    write_to_file(new_money_list, money_result_file)
    new_person_list = deal_every_persons_money(person_list, new_money_list)
    write_to_file(new_person_list, person_result_file)


if __name__ == "__main__":
    # execute only if run as a script
    calculate_money()